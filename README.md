This is one of the assignments I did in Spring 2020 for CS50.

Please find the specifications for this project [here](https://cs50.harvard.edu/x/2021/psets/5/speller/).

Speller is a program to find the misspelled words in a given text using a dictionary.

The main focus of this project was to learn memory management in C and string manupilation.
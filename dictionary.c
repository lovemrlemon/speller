// Implements a dictionary's functionality

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

#include "dictionary.h"

// Represents a node in a hash table
typedef struct node
{
    char word [LENGTH + 1];
    struct node *next;
}
node;

// Number of buckets in hash table
const unsigned int N = 33091;

// Number of words read from the dictionary
unsigned int words = 0;

// Hash table
node *table[N];

// Returns true if word is in dictionary else false
bool check(const char *word)
{
    // Go for the correct index in the table.
    unsigned int hash_val = hash(word);
    node *current = table[hash_val];

    // iterate over the linked list.
    while (current != NULL)
    {
        if (strcasecmp(word, current->word) == 0)
        {
            return true;
        }
        current = current->next;
    }
    return false;
}

// Hashes word to a number
unsigned int hash(const char *word)
{
    int product = 1;
    unsigned int hash_val;
    for (int i = 0, l = strlen(word); i < l; i++)
    {
        if (word[i] > 90)
        {
            product = product * (word[i] - 32);
        }
        else
        {
            product = product * word[i];
        }
    }
    hash_val = product % N;

    return hash_val;
}

// Loads dictionary into memory, returning true if successful else false
bool load(const char *dictionary)
{
    // opens the file.
    FILE *dic = fopen(dictionary, "r");
    if (dic == NULL)
    {
        return false;
    }
    
    // set the variables
    char buf[LENGTH + 1];
    unsigned int hash_val;
    node *new, *current;

    while (fscanf(dic, "%s", buf) == 1)
    {
        // assign values to the node.
        new = malloc(sizeof(node));
        if (new == NULL)
        {
            fclose(dic);
            return false;
        }
        
        // set values of the new node
        strcpy(new->word, buf);
        new->next = NULL;

        hash_val = hash(buf);
        
        //check for first insertion
        if (table[hash_val] == NULL)
        {
            table[hash_val] = new;
        }
        //else, loop through the list and find the last node to insert after.
        else
        {
            current = table[hash_val];
            while (current->next != NULL)
            {
                current = current->next;
            }
            current->next = new;
        }
        // keep the track of the size of the dic.
        words ++;
    }
    fclose(dic);
    return true;
}

// Returns number of words in dictionary if loaded else 0 if not yet loaded
unsigned int size(void)
{
    return words;
}

// Unloads dictionary from memory, returning true if successful else false
bool unload(void)
{
    for (int i = 0; i < N; i++)
    {
        node *current = table[i];
        while (current != NULL)
        {
            node *tmp = current->next;
            free(current);
            current = tmp;
        }
    }
    return true;
}

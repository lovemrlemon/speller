// str_slice function is from "https://stackoverflow.com/questions/26620388/c-substrings-c-string-slicing"

// 
//~~~IT'S ALIVE!!~~~~
//

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

// represents a node in the linked list
typedef struct node
{
    char *word;
    bool flag;
    struct node *next;
}
node;

// function prototypes
void print_node(node *n);

int main (void)
{
    // set the word
    char word[6] = "ALICE";
    int len = (int) strlen(word);
    char *slice;

    // set the buffer to use in slice_string
    char buffer[len + 1];

    // list of size 0
    node *list = NULL;
    
    // initialize the node
    node *new = NULL;
    node *current;
    
    for (int i = 0; i < len; i++)
    {  
        //slice the string
        slice = (char*) malloc(i+2);
        if (slice == NULL)
        {
            return 1;
        }
        strncpy(slice, word, i+1);

        // create new node
        new = malloc(sizeof(node));
        if (new == NULL)
        {
            return 1;
        }
        
        // add values to the new node
        new->word = slice;
        new->next = NULL;

        // check for first insertion
        if (list == NULL)
        {
            list = new;
        }
        else
        {
            //else, loop through the list and find the last node.
            // insert the new one after it.
            current = list;
            while (current->next != NULL)
            {
                current = current->next;
            }
            current->next = new;
        }
    }
    new->flag = true;
    
    // Print list
    for (node *tmp = list; tmp != NULL; tmp = tmp->next)
    {
        printf("%s%c  %d\n", tmp->word, '.', tmp->flag);
    }
    
    // Free list
    while (list != NULL)
    {
        node *tmp = list->next;
        free(list);
        list = tmp;
    }
}

void print_node(node *n)
{
    printf("word = %s, flag = %d", n->word, n->flag);
    if (n->next == NULL)
    {
        printf("    next is NULL\n");
    }
    else
    {
        printf("    the next item is %s\n", n->next->word);
    }
}